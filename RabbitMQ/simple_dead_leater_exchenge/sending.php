<?php

/*
     чтоб работали эти примеры нужно проверить подключен (расскомментирован) ли плагин в файле docker/rabbitmq-config/Dockerfile строки 6 - 14

    нашел решение для повторной отправки сообщений но тогда нужно сделать тип очереди = quorum
    Работает так:
    1. создаем exchange который будет принимать fail меседжи, тип директ, durable = true, у нас exchange = dlx_exchange

    2. Создаем очереди их которых хотим перехватывать фейлы, тип очередей  должен быть quorum,
        указываем в параметрах какой exchange будет обрабатывать фэйлы, указываем routing-key, без него не работает
        и указываем сколько попыток будет до перехвата delivery-limit, durable = true

    3. Создаем очередь в которую будм складывать фэйлы, dead_letter_queue. И привязываем ее к нашему exchange

    4. В sending создаются ВСЕ очереди на примере банков, 1 очередь 1 банк

    отправить php RabbitMQ/simple_dead_leater_exchenge/sending.php
    получить php RabbitMQ/simple_dead_leater_exchenge/receive.php
    получить php RabbitMQ/simple_dead_leater_exchenge/receive2.php
*/
require_once __DIR__ . '/../../vendor/autoload.php';
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

$connection = new AMQPStreamConnection('rabbitmq', 5672, 'guest', 'guest');
$channel = $connection->channel();

$testNameQueue = [
  "first",
  "second",
];

$amqpTable = [
    'x-queue-type' => 'quorum',
    'x-dead-letter-exchange' => 'dlx_exchange',
    'x-dead-letter-routing-key' => 'dlx_key',
    'x-delivery-limit' => 3];

//отмечаем в $channel->queue_declare 3 аргумент (durable) true чтоб не потерять сообщения если серевер крашнется
$channel->exchange_declare("dlx_exchange", "direct", false, true, false);
foreach ($testNameQueue as $queueName) {
    $channel->queue_declare($queueName, false, true, false, false, false,
      new \PhpAmqpLib\Wire\AMQPTable($amqpTable));
}
$channel->queue_declare('dead_letter_queue', false, true, false, false);
$channel->queue_bind('dead_letter_queue', "dlx_exchange", 'dlx_key');


$num = rand(1, 9);
$randomQueue = array_rand($testNameQueue);
$qName = $testNameQueue[$randomQueue];
$msg = new AMQPMessage($num, ['delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT]);

$channel->basic_publish($msg, '', $qName);
echo " [x] Sent in queue name: {$qName}  num = {$num} \n";

$channel->close();
$connection->close();

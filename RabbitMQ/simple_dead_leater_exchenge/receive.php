<?php

/*
    чтоб работали эти примеры нужно проверить подключен (расскомментирован) ли плагин в файле docker/rabbitmq-config/Dockerfile строки 6 - 14

    нашел решение для повторной отправки сообщений но тогда нужно сделать тип очереди = quorum
    Работает так:
    1. создаем exchange который будет принимать fail меседжи, тип директ, durable = true, у нас exchange = dlx_exchange

    2. Создаем очереди их которых хотим перехватывать фейлы, тип очередей  должен быть quorum,
        указываем в параметрах какой exchange будет обрабатывать фэйлы, указываем routing-key, без него не работает
        и указываем сколько попыток будет до перехвата delivery-limit, durable = true

    3. Создаем очередь в которую будм складывать фэйлы, dead_letter_queue. И привязываем ее к нашему exchange

    отправить php RabbitMQ/simple_dead_leater_exchenge/sending.php
    получить php RabbitMQ/simple_dead_leater_exchenge/receive.php
    получить php RabbitMQ/simple_dead_leater_exchenge/receive2.php
*/



require_once __DIR__ . '/../../vendor/autoload.php';
require_once  __DIR__ . '/../../functions/functions.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

$connection = new AMQPStreamConnection('rabbitmq', 5672, 'guest', 'guest');
$channel = $connection->channel();

$amqpTable = [
    'x-queue-type' => 'quorum',
    'x-dead-letter-exchange' => 'dlx_exchange',
    'x-dead-letter-routing-key' => 'dlx_key',
    'x-delivery-limit' => 3];

//отмечаем в $channel->queue_declare 3 аргумент (durable) true чтоб не потерять сообщения если серевер крашнется
$channel->exchange_declare("dlx_exchange", "direct", false, true, false);
$channel->queue_declare('first', false, true, false, false, false,
    new \PhpAmqpLib\Wire\AMQPTable($amqpTable));
$channel->queue_declare('dead_letter_queue', false, true, false, false);
$channel->queue_bind('dead_letter_queue', "dlx_exchange", 'dlx_key');


echo " [*]  waiting for messages. To exit press CTRL+C\n";

$callback = function ($msg) use ($channel) {
    $num = $msg->getBody();
    $deliveryCount = $msg->getDeliveryTag();
    $chan = $msg->getRoutingKey();
    $type = gettype($deliveryCount);
    if ($num % 2 == 0) {
        echo " [*]  get even num: {$msg->getBody()}, get routing key '{$msg->getRoutingKey()}', '$deliveryCount' '$type', channel: {$chan}\n";
        $msg->ack();
    } else {
        echo " [*]  get NOT even num: {$msg->getBody()}, get routing key '{$msg->getRoutingKey()}', '$deliveryCount' '$type', channel: {$chan}\n";
        $msg->reject();
        echo "reject\n";
    }

};

//$channel->basic_qos = не отправлять новое сообщение рабочему процессу, пока он не обработает и не подтвердит предыдущее.
$channel->basic_qos(null, 1, null);
//no_ack ставим false чтоб работало подтверждение
$channel->basic_consume('first', '', false, false, false, false, $callback);

while ($channel->is_open()) {
    $channel->wait();
}

$channel->close();
$connection->close();
<?php

//нашел решение для повторной отправки сообщений но тогда нужно сделать тип очереди = quorum
//рабочий пример очередь Queue_3 и задал значение 'x-delivery-limit' => 3
//так же в консьюмере нужен метод $msg->reject()

require_once __DIR__ . '/../../vendor/autoload.php';
require_once  __DIR__ . '/../../functions/functions.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

$connection = new AMQPStreamConnection('rabbitmq', 5672, 'guest', 'guest');
$channel = $connection->channel();

//отмечаем в $channel->queue_declare 3 аргумент (durable) true чтоб не потерять сообщения если серевер крашнется
//$channel->queue_declare('Queue_1', false, true, false, false, false, new \PhpAmqpLib\Wire\AMQPTable(['x-queue-type' => 'quorum', 'x-delivery-limit' => 3]));
$channel->queue_declare('Queue_3', false, true, false, false, false, new \PhpAmqpLib\Wire\AMQPTable(['x-queue-type' => 'quorum', 'x-delivery-limit' => 3]));

echo " [*] Queue_3 waiting for messages. To exit press CTRL+C\n";

$callback = function ($msg) use ($channel) {
    $num = $msg->getBody();
    if ($num % 2 == 0) {
//        деливери таг нужно похоже обнулять тк он имеет макс длину
        echo " [*] Queue_3 get even num: {$msg->getBody()}, count: {$msg->getDeliveryTag()}\n";
        $msg->ack();
    } else {
        echo " [*] Queue_3 get NOT even num: {$msg->getBody()} count: {$msg->getDeliveryTag()}\n";
        $msg->reject();
        echo "reject\n";
    }

};

//$channel->basic_qos = не отправлять новое сообщение рабочему процессу, пока он не обработает и не подтвердит предыдущее.
$channel->basic_qos(null, 1, null);
//no_ack ставим false чтоб работало подтверждение
$channel->basic_consume('Queue_3', '', false, false, false, false, $callback);

while ($channel->is_open()) {
    $channel->wait();
}

$channel->close();
$connection->close();
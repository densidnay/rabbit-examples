<?php

//нашел решение для повторной отправки сообщений но тогда нужно сделать тип очереди = quorum для другого типа не работает
//рабочий пример очередь Queue_3 и задал значение 'x-delivery-limit' => 3
//так же в консьюмере нужен метод $msg->reject()

require_once __DIR__ . '/../../vendor/autoload.php';
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

$connection = new AMQPStreamConnection('rabbitmq', 5672, 'guest', 'guest');
$channel = $connection->channel();

//отмечаем в $channel->queue_declare 3 аргумент (durable) true чтоб не потерять сообщения если серевер крашнется
//$channel->queue_declare('Queue_1', false, true, false, false, false, new \PhpAmqpLib\Wire\AMQPTable(['x-queue-type' => 'quorum', 'x-delivery-limit' => 3]));
$channel->queue_declare('Queue_3', false, true, false, false, false, new \PhpAmqpLib\Wire\AMQPTable(['x-queue-type' => 'quorum', 'x-delivery-limit' => 3]));



$num = rand(1, 9);
//$num = "foo test";
//$num = 5;

$msg = new AMQPMessage($num, ['delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT]);

$channel->basic_publish($msg, '', 'Queue_3');
echo " [x] Sent in 'dead'  num = {$num} \n";

$channel->close();
$connection->close();

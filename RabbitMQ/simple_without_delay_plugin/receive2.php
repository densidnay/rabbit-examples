<?php

/*
    паттерн delay message без плагина
    sending.php создает обменники, очереди и биндит их
    потом отправляет в каждую очередь случайное сообщение с натуральным числом

    receive.php слушает очередь "otp", в случае если пришло число четное, убирает его из очереди
    если пришло не четное, делаем reject и сообщение попадает через обменник "fail",
    в очередь "inbox.retry.dlx" где через 'x-message-ttl' сек возвращается на повторную попытку в очередь "otp"

    receive2.php слушает очередь "tcs", в случае если пришло число четное, убирает его из очереди
    если пришло не четное, делаем reject и сообщение попадает через обменник "fail",
    в очередь "inbox.retry.dlx" где через 'x-message-ttl' сек возвращается на повторную попытку в очередь "tcs"
*/



require_once __DIR__ . '/../../vendor/autoload.php';
require_once  __DIR__ . '/../../functions/functions.php';

use App\RabbitClass\Rabbit;
use App\RabbitConnect\Connect;
use PhpAmqpLib\Message\AMQPMessage;

$rabbit = new Rabbit(new Connect());
$rabbit->declare();

$num = rand(1, 9);
$msg = new AMQPMessage($num, ['delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT]);

echo " [*]  waiting for messages. To exit press CTRL+C\n";
$queues = $rabbit->getQueues();
$callback = function ($msg) use ($queues) {
    $num = $msg->getBody();
    if ($num % 2 == 0) {
        echo " [*]  get even num: {$msg->getBody()}, get routing key '{$msg->getRoutingKey()}', queue name: '{$queues[1]}'\n";
        $msg->ack();
        echo "ack\n";
    } else {
        echo " [*]  get odd num: {$msg->getBody()}, get routing key '{$msg->getRoutingKey()}, queue name: '{$queues[1]}'\n";
        $msg->reject(false); // false для того чтоб сработал dlx
        echo "reject\n";
    }

};

$rabbit->consume($queues[1], $callback);
<?php

/*
    паттерн delay message без плагина
    sending.php создает обменники, очереди и биндит их
    потом отправляет в каждую очередь случайное сообщение с натуральным числом

    receive.php слушает очередь "otp", в случае если пришло число четное, убирает его из очереди
    если пришло не четное, делаем reject и сообщение попадает через обменник "fail",
    в очередь "inbox.retry.dlx" где через 'x-message-ttl' сек возвращается на повторную попытку в очередь "otp"

    receive2.php слушает очередь "tcs", в случае если пришло число четное, убирает его из очереди
    если пришло не четное, делаем reject и сообщение попадает через обменник "fail",
    в очередь "inbox.retry.dlx" где через 'x-message-ttl' сек возвращается на повторную попытку в очередь "tcs"
*/


require_once __DIR__ . '/../../vendor/autoload.php';

use App\RabbitClass\Rabbit;
use App\RabbitConnect\Connect;
use PhpAmqpLib\Message\AMQPMessage;

$rabbit = new Rabbit(new Connect());
$rabbit->declare();

$num = rand(1, 9);
$msg = new AMQPMessage($num, ['delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT]);

foreach ($rabbit->getQueues() as $routingKey) {
    $rabbit->sendMessage($msg, Rabbit::START_EXCHANGE, $routingKey);
}

$rabbit->close();
<?php

/*
пример работы плагина delay message
создаем обменник с типом x-delayed-message, и с аргументом "x-delayed-type" => "direct"
создаем нужное количество очередей, в примере их две first_delay и second_delay
привязываем эти очереди (queue_bind) к нашему обменнику, указывая имя очереди, имя обменника и ключ очереди.
отправляем сообщение указав в заголовке задержку в милисекундах 'x-delay' => 10000
отправляем в обменник указав роут
каждый консьюмер просто слушает свою очередь
отправка сообщения в рандомнуб очередь make send_delay
получение сообщения make receive_delay_first
получение сообщения make receive_delay_second
*/

require_once __DIR__ . '/../../vendor/autoload.php';
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Wire\AMQPTable;

$connection = new AMQPStreamConnection('rabbitmq', 5672, 'guest', 'guest');
$channel = $connection->channel();

$testNameQueue = [
  "first_delay",
  "second_delay",
];

$channel->exchange_declare("delay_exchange", "x-delayed-message", false, true, false, false, false, new AMQPTable(["x-delayed-type" => "direct"]));
$channel->queue_declare('first_delay', false, false, false, false);
$channel->queue_declare('second_delay', false, false, false, false);
//$channel->queue_bind('first_delay','delay_exchange', 'first_delay'); // не пойму зачем нужен 3 параметр
$channel->queue_bind('first_delay','delay_exchange', 'first_delay');
$channel->queue_bind('second_delay','delay_exchange', 'second_delay');

    $randomQueue = array_rand($testNameQueue);
    $qName = $testNameQueue[$randomQueue];
    $key = rand();
    $date = date("Y-m-d H:i:s");
    $textMessage = "Hello World_{$key}, send time: {$date}, queue name: {$qName}";

    $msg = new AMQPMessage($textMessage, ['delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT, 'application_headers' => new AMQPTable(['x-delay' => 10000])]);
    $channel->basic_publish($msg, 'delay_exchange', $qName);

    echo " [x] Sent '{$textMessage}'\n";

$channel->close();
$connection->close();

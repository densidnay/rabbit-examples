<?php

/*
пример работы плагина delay message
создаем обменник с типом x-delayed-message, и с аргументом "x-delayed-type" => "direct"
создаем нужное количество очередей, в примере их две first_delay и second_delay
привязываем эти очереди (queue_bind) к нашему обменнику, указывая имя очереди, имя обменника и ключ очереди.
отправляем сообщение указав в заголовке задержку в милисекундах 'x-delay' => 10000
отправляем в обменник указав роут
каждый консьюмер просто слушает свою очередь
отправка сообщения в рандомнуб очередь make send_delay
получение сообщения make receive_delay_first
получение сообщения make receive_delay_second
*/

require_once __DIR__ . '/../../vendor/autoload.php';
use PhpAmqpLib\Connection\AMQPStreamConnection;

$connection = new AMQPStreamConnection('rabbitmq', 5672, 'guest', 'guest');
$channel = $connection->channel();

$channel->queue_declare('second_delay', false, false, false, false);

$date = date("Y-m-d H:i:s");
echo " [{$date}] Waiting for messages. To exit press CTRL+C\n";

$callback = function ($msg) {
    $date = date("Y-m-d H:i:s");
    echo " [{$date}] Received ", $msg->body, "\n";
};

$channel->basic_consume('second_delay', '', false, true, false, false, $callback);

while ($channel->is_open()) {
    $channel->wait();
}

$channel->close();
$connection->close();
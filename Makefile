#запуск докер приложений
build:
	docker-compose up --build -d

#остановка докер приложений
stop:
	docker-compose down

all-stop:
	docker stop $(docker container ls -qa)

#запуск композера внутри докера
install:
	docker exec -it rabbit-php-fpm composer install

#заходим в bash
bash:
	docker exec -it rabbit-example-php-fpm bash

#заходим в rabbit
#команда внтури раббита на проверку сообщений отправленых, подтвержденных
#rabbitmqctl list_queues name messages_ready messages_unacknowledged
bash-rabbit:
	docker exec -it rabbit-example-rabbitmq bash

#===================  docker simple =====================================
receive_s_1:
	docker exec -it rabbit-php-fpm  php RabbitMQ/simple_1/receive.php

sending_s_1:
	 docker exec -it rabbit-php-fpm  php RabbitMQ/simple_1/sending.php

#===================  my simple =====================================
#отправляем сообщение в очередь А или В
my_sending_simple:
	 docker exec -it rabbit-php-fpm  php RabbitMQ/mySimple_1/sending.php

#получаем сообщение из очереди А
receive_A:
	 docker exec -it rabbit-php-fpm  php RabbitMQ/mySimple_1/receiveA.php

#получаем сообщение из очереди В
receive_B:
	 docker exec -it rabbit-php-fpm  php RabbitMQ/mySimple_1/receiveB.php

# =======================================================================dead leater exchange ================================
#отправка в очередь
send_dead:
	docker exec -it rabbit-example-php-fpm php RabbitMQ/simple_dead_leater_exchenge/sending.php

#получаем сообщение из очереди 1
receive_dead_1:
	docker exec -it rabbit-example-php-fpm php RabbitMQ/simple_dead_leater_exchenge/receive.php

#получаем сообщение из очереди 2
receive_dead_2:
	docker exec -it rabbit-example-php-fpm php RabbitMQ/simple_dead_leater_exchenge/receive2.php

#получаем сообщение из очереди dead_letter_queue
receive_dead_letter_queue:
	docker exec -it rabbit-example-php-fpm php RabbitMQ/simple_dead_leater_exchenge/receive_dead_leater.php

# =======================================================================dead leater exchange ================================
# ======================================================================= dealy msg ================================

send_delay:
	docker exec -it rabbit-example-php-fpm php RabbitMQ/simple_delay_queue/sending.php

receive_delay_first:
	docker exec -it rabbit-example-php-fpm php RabbitMQ/simple_delay_queue/receive_first.php

receive_delay_second:
	docker exec -it rabbit-example-php-fpm php RabbitMQ/simple_delay_queue/receive_second.php

# ======================================================================= dealy msg ================================

#так можно посмотреть пути к логам, конфигу, и другую инфу
rabbit-status:
	docker exec -it rabbit-example-rabbitmq rabbitmq-diagnostics status

#смотрим ошибки кролика в докере
docker-rabbit-error:
	docker logs rabbit-example-rabbitmq

#смотрим окружение кролика в докере
docker-rabbit-inspect:
	docker inspect rabbit-example-rabbitmq


rabbit-conteiner-log:
	docker-compose logs -f rabbitmq


#=============================================================================

rabbit-without-plugin-publish:
	 docker exec -it rabbit-example-php-fpm php RabbitMQ/simple_without_delay_plugin/sending.php

rabbit-without-plugin-consume:
	 docker exec -it rabbit-example-php-fpm php RabbitMQ/simple_without_delay_plugin/receive.php

rabbit-without-plugin-consume2:
	 docker exec -it rabbit-example-php-fpm php RabbitMQ/simple_without_delay_plugin/receive2.php
<?php

namespace App\RabbitClass;

use App\RabbitConnect\Connect;
use PhpAmqpLib\Connection\AMQPStreamConnection;

class Rabbit
{
    const START_EXCHANGE = "start_exchange";

    const FAIL_EXCHANGE = "fail";

    const RETRY_DLX_QUEUE = "inbox.retry.dlx";

    const DIRECT_TYPE = "direct";

    protected AMQPStreamConnection $connection;

    protected $channel;

    protected array $queues = [
      "otp",
      "tcs",
    ];

    protected array $queueSettings = [
      'x-dead-letter-exchange' => 'fail',
    ];

    protected array $retryQueueSettings = [
      'x-message-ttl' => 15000,
      'x-dead-letter-exchange' => self::START_EXCHANGE,
      'x-queue-mode' => 'lazy',
    ];

    public function __construct(Connect $connect)
    {
        $this->connection = $connect->getConnection();
        $this->channel = $this->connection->channel();
    }

    public function getQueues(): array
    {
        return $this->queues;
    }

    protected function getQueueSettings(): array
    {
        return $this->queueSettings;
    }

    protected function getRetryQueueSettings(): array
    {
        return $this->retryQueueSettings;
    }

    public function declare()
    {
        // создали обменник для дефолтных очередей
        $this->channel->exchange_declare(self::START_EXCHANGE, self::DIRECT_TYPE, false, true, false);

        // создали фэйл обменник для отправки в фэйл очередь
        $this->channel->exchange_declare(self::FAIL_EXCHANGE, self::DIRECT_TYPE, false, true, false);

        //отмечаем в $this->channel->queue_declare 3 аргумент (durable) true чтоб не потерять сообщения если серевер крашнется
        // фэйл очередь где храняться все фэйл сообщения, которые лежат тут 'x-message-ttl' секунд и потом уходят
        // на повторную отправку в обменник self::START_EXCHANGE и далее в ту очередь из которой пришли
        $this->channel->queue_declare(
          self::RETRY_DLX_QUEUE,
          false,
          true,
          false,
          false,
          false,
          new \PhpAmqpLib\Wire\AMQPTable($this->getRetryQueueSettings())
        );

        // объявляем дефолтные очереди, одна очередь = один банк,
        // биндим сразу очередь к обменнику self::START_EXCHANGE,
        // и биндим в фэйл очередь ключ от дефолтной очереди
        foreach ($this->getQueues() as $queueName) {
            $this->channel->queue_declare(
              $queueName,
              false,
              true,
              false,
              false,
              false,
              new \PhpAmqpLib\Wire\AMQPTable($this->getQueueSettings())
            );
            $this->channel->queue_bind($queueName, self::START_EXCHANGE, $queueName);
            $this->channel->queue_bind(self::RETRY_DLX_QUEUE, self::FAIL_EXCHANGE, $queueName);
        }
    }

    public function sendMessage($msg, $exchange = '', $routingKey = '')
    {
        $this->channel->basic_publish($msg, $exchange, $routingKey);
        $payload = $msg->getBody();
        echo " [x] Sent in queue name: {$routingKey}  num = {$payload} \n";
    }

    public function close()
    {
        $this->channel->close();
        $this->connection->close();

    }

    public function consume($queue, $callback, $consumerTag = '')
    {
        //$channel->basic_qos = не отправлять новое сообщение рабочему процессу, пока он не обработает и не подтвердит предыдущее.
        $this->channel->basic_qos(null, 1, null);
        //no_ack ставим false чтоб работало подтверждение
        $this->channel->basic_consume($queue, $consumerTag, false, false, false, false, $callback);

        while ($this->channel->is_open()) {
            $this->channel->wait();
        }

        $this->close();
    }
}
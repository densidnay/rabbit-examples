<?php

namespace App\RabbitConnect;

use PhpAmqpLib\Connection\AMQPStreamConnection;

class Connect
{

    const HOST = "rabbitmq";

    const PORT = 5672;

    const USER = "guest";

    const PASSWORD = "guest";

    const VHOST = "/";

    const INSIST = false;

    const LOGIN_METHOD = "AMQPLAIN";

    const LOGIN_RESPONSE = null;

    const LOCALE = "en_US";

    const CONNECTION_TIMOUT = 3.0;

    const READ_WRITE_TIMEOUT = 3.0;

    const CONTEXT = null;

    const KEEPALIVE = false;

    const HEARTBEAT = 12;

    protected AMQPStreamConnection $connection;

//    по сути для работы достаточно первых 4 парамметров, но для HEARTBEAT надо столько. С пхп 8 можно сделать проще
    // понятно что креды надо тянуть из ENV и т.п.
    public function __construct()
    {
        $this->connection = new AMQPStreamConnection(
          self::HOST,
          self::PORT,
          self::USER,
          self::PASSWORD,
          self::VHOST,
          self::INSIST,
          self::LOGIN_METHOD,
          self::LOGIN_RESPONSE,
          self::LOCALE,
          self::CONNECTION_TIMOUT,
          self::READ_WRITE_TIMEOUT,
          self::CONTEXT,
          self::KEEPALIVE,
          self::HEARTBEAT
        );
    }

    public function getConnection(): AMQPStreamConnection
    {
        return $this->connection;
    }

}